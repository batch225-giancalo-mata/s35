// 1. Create a User schema.
// 2. Create a User model.
// 3. Create a POST route that will access the "/signup" route that will create a user.
// 4. Process a POST request at the "/signup" route using postman to register a user.
// 5. Create a git repository named S35.
// 6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 7. Add the link in Boodle.


const express = require("express");


const mongoose = require("mongoose");


const app = express();
const port = 5003;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://giancarlomata888:Eminem88088@cluster0.nezwiao.mongodb.net/?retryWrites=true&w=majority",
	{
		// In simple words, "useNewUrlParser : true" allows us to avoid any current and future errors while connecting to MongoDB.

		useNewUrlParser : true,
		
		// False by default. Set to true to opt in to using MongoDB driver's new connection management engine. You should set this option to true, except for the unlikely case that it prevents you from maintaining a stable connection.
		useUnifiedTopology : true
	}

);	

// Set notification for connection success or failure in MongoDB

let db = mongoose.connection;

// console.error.bind(console, "comments") allows us to print errors in the browser console and in the terminal.

// Notification for error
db.on("error", console.error.bind(console, "connection error"));

// Notification for Connected
db.on('open', () => console.log("Connected to MongoDB!"));


// [SECTION] Mongoose Schemas

// Schemas act as  blueprints to our database which describes how the data may relate to other tables or other data models. However, the schema does not actually contain data.
// The 'new' keyword creates a new Schema

const taskSchema = new mongoose.Schema({ 
	
	// Define the fields with the corresponding data type
	// For a task, it needs a "task name" and "task status"
	
	name : String,
	status : { 
		type : String,
		default: "pending"
	}
});

	// There is a field called "name" and its data type is "String"
	// There is a field called "status" that is a "String" and the default value is "pending"
	// Default values are the predefined values for a field if we don't put any value

// [SECTION] Models
// Uses schemas and are to create/instantiate objects that correspond to the schema.
// Models use Schema and they act as the middleman from the server (JS CODE) to our database.

// The variable/object "Tasks" can now be used to run commands for interacting with our database.
// "Task" is capitalized following "Model, View & Control" (MVC) approach for naming conventions.
// Models must be in singular form and CAPITALIZED(first letter).

// For the first parameter of the Mongoose Model method indicates the collection where to store the data.

// for Second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection.

// Using Mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into plural form when creating a collection in postman.

const Task = mongoose.model("Task", taskSchema);

let users = [];

app.post("/signup", (req, res) => {

	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== ''){

		// This will send a response back to the Client/Postman to the users array created above.

		users.push(req.body);

		// This will response in the Client/Postman

		res.send(`User ${req.body.username} successfully registered`);
	} else {

		// If the username and password are not complete an error message will be sent back to Client/Postman

		res.send("Please input BOTH username and password.")
	}

}); 
// Getting all the tasks

// Business Logic
/*
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/users", (req, res) => {

	// "find" is a Mongoose method that is similar to Mongodb "find", and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
	Task.find({}, (err, result) => {

		// If an error occurred
		if (err) {

			// Will print any errors found in the console
			return console.log(err);

		// If no errors are found
		} else {

			// Status "200" means that everything is "OK" in terms of processing
			// The "json" method allows to send a JSON format for the response
			// The returned response is purposefully returned as an object with the "data" property to mirror real world complex data structures
			return res.status(200).json({
				data : result			
			})

		}

	})
});


app.listen(port, () => console.log(`Server running at port ${port}!`))

